from dataclasses import dataclass


# dataclass takes care of a lot of boilerplate.
# It creates __eq__, __repr__ and maybe other convenience methods
@dataclass
class Item(object):
    name: str
    sell_in: int
    quality: int
