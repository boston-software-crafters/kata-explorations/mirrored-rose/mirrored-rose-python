from typing import List

from mirrored_rose.inventory import update_item
from mirrored_rose.legacy.item import Item


class MirroredRose(object):
    def __init__(self, items: List[Item]):
        self.items = items[:]

    def update_items(self):
        for item in self.items:
            update_item(item)
