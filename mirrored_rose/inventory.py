from mirrored_rose.legacy.item import Item


AGED_BRIE = "Aged Brie"
SULFURAS = "Sulfuras, Hand of Ragnaros"
BACKSTAGE_PASS = "Backstage"

NORMAL_DECAY_RATE = 1
BRIE_DECAY_RATE = -1 * NORMAL_DECAY_RATE
QUALITY_FLOOR = 0
QUALITY_CEILING = 50
EXPIRED_MULTIPLIER = 2
BACKSTAGE_5_DECAY_RATE = -3
BACKSTAGE_10_DECAY_RATE = -2
BACKSTAGE_DECAY_RATE = -1 * NORMAL_DECAY_RATE


def update_item(item: Item):
    if is_not_sulfuras(item):
        update_sellin(item)
        update_quality(item)


def get_degradation_rate(item: Item):
    degradation_rate = NORMAL_DECAY_RATE
    if is_aged_brie(item):
        degradation_rate = BRIE_DECAY_RATE

    if is_passed_sellin(item):
        degradation_rate *= EXPIRED_MULTIPLIER
    return degradation_rate


def is_passed_sellin(item: Item):
    return item.sell_in < 0


def update_quality(item: Item):
    if is_backstage_pass(item):
        if is_passed_sellin(item):
            item.quality = 0
            return

        degradation_rate = get_backstage_passes_degradation_rate(item)
    else:
        degradation_rate = get_degradation_rate(item)

    item.quality -= degradation_rate
    quality_clamp(item)


def get_backstage_passes_degradation_rate(item: Item):
    if item.sell_in <= 5:
        return BACKSTAGE_5_DECAY_RATE
    elif item.sell_in <= 10:
        return BACKSTAGE_10_DECAY_RATE
    else:
        return BACKSTAGE_DECAY_RATE


def quality_clamp(item: Item):
    # TODO: Rename in 6 months from 1/11/21
    item.quality = max(item.quality, QUALITY_FLOOR)
    item.quality = min(item.quality, QUALITY_CEILING)


def update_sellin(item: Item):
    item.sell_in -= 1


def is_not_sulfuras(item: Item) -> bool:
    return item.name != SULFURAS


def is_aged_brie(item: Item) -> bool:
    return item.name == AGED_BRIE


def is_backstage_pass(item: Item) -> bool:
    return BACKSTAGE_PASS in item.name
