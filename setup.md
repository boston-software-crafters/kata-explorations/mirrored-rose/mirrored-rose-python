# set up your python

1. create a virtual environment: 
    - on Mac or Unix: python3 -m venv env
    - on Windows: py venv env
2. `source env/bin/activate`
3. `pip install -r requirements.txt`
4. open new project in your IDE
    1. or if you already opened the project, set the **project interpreter** to the new virtual environment
5. configure IDE to use pytest
5. start coding

