## Inventory Policies

 * All items have a SellIn value which denotes the 
 number of days we have to sell the item
 * All items have a Quality value which denotes how 
 valuable the item is 
 
### Policy #1
 
 * At the end of each day our system lowers both values 
 for every item
 
### Policy #2

 * Once the sell by date has passed, Quality degrades 
twice as fast

### Policy #3

 * The Quality of an item is never negative

### Policy #4

 * "Aged Brie" actually increases in Quality the older it gets

### Policy #5

 * The Quality of an item is never more than 50

### Policy #6

 * "Sulfuras", being a legendary item, never has to 
 be sold or decreases in Quality; the quality of
 Sulfuras may be anywhere between 20 and 100 (inclusive).

### Policy #7

 * "Backstage passes”, like aged brie, increases in Quality 
as it’s SellIn value approaches; Quality increases by 2 when 
there are 10 days or less and by 3 when there are 5 days or 
less but Quality drops to 0 after the concert

### Policy #8

 * "Conjured" items degrade in Quality twice as fast as 
normal items
