import pytest

from mirrored_rose.legacy.item import Item
from mirrored_rose.legacy.mirrored_rose import MirroredRose
from tests.utilities import ItemName

@pytest.mark.parametrize(
    "description, name_, before_sell_in, before_quality, after_quality",
    [
        ('Policy #2 NA',ItemName.VEST, 10, 20, 19),
        ('Policy #2 NA',ItemName.ELIXIR, 10, 1, 0),
        ('Policy #2 TEST',ItemName.VEST, 0, 20, 18),
        ('Policy #3',ItemName.ELIXIR, 10, 0, 0),
        ('Policy #3',ItemName.ELIXIR, 0, 0, 0),
        ('Policy #4',ItemName.BRIE, 10, 20, 21),
        ('Policy #4 AND #2',ItemName.BRIE, -2, 43, 45),
        ('Policy #4',ItemName.BRIE, 10, 47, 48),
        ('Policy #4',ItemName.BRIE, 10, 48, 49),
        ('Policy #5 NA',ItemName.BRIE, 10, 49, 50),
        ('Policy #5 TEST',ItemName.BRIE, 10, 50, 50),
        ('Policy #5 NA',ItemName.BRIE, -2, 47, 49),
        ('Policy #5 NA',ItemName.BRIE, -2, 48, 50),
        ('Policy #5 TEST',ItemName.BRIE, -2, 49, 50),
        ('Policy #5 TEST',ItemName.BRIE, -2, 50, 50),
        ('Policy #6',ItemName.SULFURAS, 10, 20, 20),
        ('Policy #6',ItemName.SULFURAS, 10, 100, 100),
        ('Policy #7',ItemName.TICKETS, 15, 20, 21),
        ('Policy #7 AND #5',ItemName.TICKETS, 15, 50, 50),
        ('Policy #7',ItemName.TICKETS, 10, 20, 22),
        ('Policy #7',ItemName.TICKETS, 5, 20, 23),
        ('Policy #7',ItemName.TICKETS, 0, 20, 0),
        ('Policy #8', ItemName.CAKE, 15, 20, 18),
        ('Policy #8 AND #2', ItemName.CAKE, 0, 20, 16),
        ('Policy #8 AND #3', ItemName.CAKE, 0, 4, 0),
        ('Policy #8 AND #3', ItemName.CAKE, 0, 3, 0),
    ],
)



def test_quality(
    description, name_, before_sell_in, before_quality, after_quality
):
    item = Item(name=name_, sell_in=before_sell_in, quality=before_quality)
    rose = MirroredRose([item])
    rose.update_items()

    assert after_quality == item.quality
