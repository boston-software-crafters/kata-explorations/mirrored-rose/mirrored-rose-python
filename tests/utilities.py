class ItemName(object):
    VEST = "+5 Dexterity Vest"
    ELIXIR = "Elixir of the Mongoose"
    BRIE = "Aged Brie"
    SULFURAS = "Sulfuras, Hand of Ragnaros"
    TICKETS = "Backstage passes to a TAFKAL80ETC concert"
    CAKE = "Conjured Mana Cake"
