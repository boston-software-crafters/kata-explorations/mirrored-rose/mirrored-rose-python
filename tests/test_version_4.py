import pytest

from mirrored_rose.legacy.item import Item
from mirrored_rose.legacy.mirrored_rose import MirroredRose
from tests.utilities import ItemName


@pytest.mark.skip(reason="not implemented")
@pytest.mark.parametrize(
    "name_, before_sell_in, before_quality, after_sell_in, after_quality",
    [
        # new behavior
        (ItemName.BRIE, 10, 20, 9, 21),
        (ItemName.BRIE, -2, 43, -3, 45),
        (ItemName.BRIE, 10, 47, 9, 48),
        (ItemName.BRIE, 10, 48, 9, 49),
        (ItemName.BRIE, 10, 49, 9, 50),
        (ItemName.BRIE, 10, 50, 9, 51),
        (ItemName.BRIE, -2, 47, -3, 49),
        (ItemName.BRIE, -2, 48, -3, 50),
        (ItemName.BRIE, -2, 49, -3, 51),
        (ItemName.BRIE, -2, 50, -3, 52),
        # old behavior
        (ItemName.VEST, 10, 20, 9, 19),
        (ItemName.ELIXIR, 10, 1, 9, 0),
        (ItemName.VEST, 0, 20, -1, 18),
        (ItemName.ELIXIR, 10, 0, 9, 0),
        (ItemName.ELIXIR, 0, 0, -1, 0),
        (ItemName.SULFURAS, 10, 20, 9, 19),
        (ItemName.SULFURAS, 10, 50, 9, 49),
        (ItemName.SULFURAS, 10, 100, 9, 99),
        (ItemName.TICKETS, 15, 20, 14, 19),
        (ItemName.TICKETS, 15, 50, 14, 49),
        (ItemName.TICKETS, 10, 20, 9, 19),
        (ItemName.TICKETS, 5, 20, 4, 19),
        (ItemName.TICKETS, 0, 20, -1, 18),
        (ItemName.CAKE, 15, 20, 14, 19),
        (ItemName.CAKE, 0, 20, -1, 18),
    ],
)
def test_version_four(
    name_, before_quality, before_sell_in, after_quality, after_sell_in
):
    expected = Item(name=name_, sell_in=after_sell_in, quality=after_quality)
    actual = Item(name=name_, sell_in=before_sell_in, quality=before_quality)
    rose = MirroredRose([actual])
    rose.update_items()

    assert actual == expected
