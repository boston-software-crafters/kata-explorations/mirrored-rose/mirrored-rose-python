import pytest

from mirrored_rose.legacy.item import Item
from mirrored_rose.legacy.mirrored_rose import MirroredRose


def test_item():
    item = Item(name="z", sell_in=1, quality=2)
    assert item.name == "z"
    assert item.sell_in == 1
    assert item.quality == 2


@pytest.mark.parametrize("name_", ["", "a", "abc"])
@pytest.mark.parametrize("sell_in", [-1, 0, 1])
@pytest.mark.parametrize("quality", [0, 1, 10])
def test_item_eq_true(name_, sell_in, quality):
    assert Item(name_, sell_in, quality) == Item(name_, sell_in, quality)


def test_item_eq_false():
    name_ = "hi there"
    sell_in = 2
    quality = 5
    control = Item(name_, sell_in, quality)
    assert control != Item(name_ + "a", sell_in, quality)
    assert control != Item(name_, sell_in + 5, quality)
    assert control != Item(name_, sell_in, quality + 1)


def test_item_mutable():
    item = Item(name="z", sell_in=1, quality=2)
    item.name += "x"
    item.sell_in += 1
    item.quality += 2
    assert item == Item("zx", 2, 4)


def test_mirrored_rose():
    first = Item("a", 1, 2)
    second = Item("b", 3, 4)
    items = [first, second]
    shop = MirroredRose(items)
    # update_items does not error
    shop.update_items()
    assert shop.items == [first, second]
    # the lists are not the same but point to the same objects
    assert shop.items is not items
    assert shop.items[0] is items[0]
    assert shop.items[1] is items[1]
